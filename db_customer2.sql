create database db_customer2

use db_customer2

create table tbl_customer(
	id int identity primary key,
	Name char(50),
	Phone_Number char(15),
	Address varchar(400),
	Email char(100),
	Description varchar(400)
);

select * from tbl_customer;